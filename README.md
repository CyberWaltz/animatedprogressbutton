Для корректной работы необходимо добавить следующие зависимости

```groovy
dependencies {
    compile 'com.github.dmytrodanylyk.android-process-button:library:1.0.4'
    compile 'com.dz.progressbuttonanimated:progressbuttonanimated:0.1'
}
```