package com.dz.animatedprogressbutton

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dz.progressbuttonanimated.ProgressButtonLib

class MainActivity : AppCompatActivity() {
    private lateinit var progressButtonLib: ProgressButtonLib
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressButtonLib = findViewById(R.id.progress_button_lib_full)
    }
}
